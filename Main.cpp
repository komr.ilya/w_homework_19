#include <iostream>
#define DEBUG


class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Agrrr!" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!" << std::endl;
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Mooo!" << std::endl;
	}
};


int main()
{
	Animal* anAnimal = new Animal;
	Animal* aDog = new Dog;
	Animal* aCat = new Cat;
	Animal* aCow = new Cow;


	/*anAnimal->Voice();
	aDog->Voice();
	aCat->Voice();
	aCow->Voice();*/

	Animal* Animals[3] = { aDog, aCat, aCow };

	for (auto element : Animals)
	{
		element->Voice();
	}

	for (int i = 0; i < 3; i++)
	{
		Animals[i]->Voice();
	}

	return 0;
}